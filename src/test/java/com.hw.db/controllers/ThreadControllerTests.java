package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;

import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import java.sql.Timestamp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.Arrays;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;


class ThreadControllerTests {
    private Vote vote;
    private User user;
    private Post post;
    private Thread thread;
    private ThreadController threadController;

    @BeforeEach
    void setUp() {
        vote = new Vote("author", 1);
        user = new User("author", "email", "fullname", "about");
        post = new Post("author", new Timestamp(0), "forum", "message", 0, 0, false);

        thread = new Thread("author", new Timestamp(0), "forum", "message", "slug", "title", 0);
        thread.setId(0);

        threadController = new ThreadController();
    }

    @Test
    @DisplayName("Check by ID test")
    void testCheckById() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(0))
                .thenReturn(thread);
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug("0"));
        }
    }

    @Test
    @DisplayName("Check by slug test")
    void testCheckBySlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug("slug"));
        }
    }

    @Test
    @DisplayName("Create post test")
    void testCreatePost() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
            MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)
        ) {
            threadMock.when(() -> ThreadDAO.getThreadById(0))
                .thenReturn(thread);
            userMock.when(() -> UserDAO.Info(post.getAuthor()))
                .thenReturn(user);

            ResponseEntity response = threadController.createPost("0", Arrays.asList(post));
            Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
            Assertions.assertEquals(Arrays.asList(post), response.getBody());
        }
    }

    @Test
    @DisplayName("Posts test")
    void testPosts() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
        ) {
            threadMock.when(() -> ThreadDAO.getThreadById(0))
                .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getPosts(thread.getId(), 1, 0, "flat", false))
                .thenReturn(Arrays.asList(post));

            ResponseEntity response = threadController.Posts("0", 1, 0, "flat", false);
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            Assertions.assertEquals(Arrays.asList(post), response.getBody());
        }
    }

    @Test
    @DisplayName("Change test")
    void testChange() {
        Thread changedThread = new Thread(
            "author", new Timestamp(0), "forum", "message", "changedSlug", "title", 0
        );
        changedThread.setId(1);

        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
        ) {
            threadMock.when(() -> ThreadDAO.getThreadById(0))
                .thenReturn(thread);

            ResponseEntity response = threadController.change("0", changedThread);
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        }
    }

    @Test
    @DisplayName("Info test")
    void testInfo() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)
        ) {
            threadMock.when(() -> ThreadDAO.getThreadById(0))
                .thenReturn(thread);

            ResponseEntity response = threadController.info("0");
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            Assertions.assertEquals(thread, response.getBody());
        }
    }

    @Test
    @DisplayName("Create vote test")
    void testCreateVote() {
        try (
            MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
            MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)
        ) {
            threadMock.when(() -> ThreadDAO.getThreadById(0))
                .thenReturn(thread);
            userMock.when(() -> UserDAO.Info(vote.getNickname()))
                .thenReturn(user);

            ResponseEntity response = threadController.createVote("0", vote);
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        }
    }
}
